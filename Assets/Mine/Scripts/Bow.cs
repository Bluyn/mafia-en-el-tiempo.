﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : MonoBehaviour {
    [SerializeField] Arrow arrow;
    [SerializeField] float offset, velocity;
    Animator anim;
    [SerializeField] bool fireTest;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (fireTest)
        {
            FireArrow();
            fireTest = false;
        }
	}
    public void FireBow()
    {
        anim.SetTrigger("Fire");
    }
    public void FireArrow()
    {
        {
            Instantiate(arrow, transform.position +( transform.forward * offset), transform.rotation, null);
            
        }
    }
}
